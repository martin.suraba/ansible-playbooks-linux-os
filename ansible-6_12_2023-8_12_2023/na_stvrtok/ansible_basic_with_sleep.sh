#!/bin/bash

#time for ansible healthcheck


yum install ansible -y

ansible all --list-hosts

sleep 10

ansible all -m ping

sleep 10
ansible all -a "free -h"

sleep 10
ansible all -a "df -hT"
sleep 10
ansible all -a "uptime"
sleep 10
ansible all -a "cat /etc/os-release"
sleep 10
ansible all -a "ip a s"
sleep 10
ansible all -a "ip r s"
sleep 10
ansible all -a "systemctl status sshd"

