#!/bin/bash

# Nainstaluj základní balíčky
yum install -y vim wget curl net-tools

# sprav update os

yum update -y 

# Nastav čas a datum
timedatectl set-timezone Europe/Prague

# nastav klavesnicu

loadkeys us 

# zmen hostname 

echo "server4" > /etc/hostname 

#nastav uvodnu hlasku
echo "hello, this is server4" > /etc/motd 


# odstran ulohu z cronu

crontab -r
