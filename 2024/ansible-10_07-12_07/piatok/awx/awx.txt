1) install docker : (from repository of docker) and start docker
 yum install -y yum-utils device-mapper-persistent-data lvm2 ansible git python-devel python-pip python-docker-py vim-enhanced
 yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
 
 yum -y install docker-ce 
 systemctl start docker
 
 systemctl enable docker
 
 2) install python3 devoleper packages 
 
 
yum install libselinux-python3 -y
yum install git gcc libffi-devel python3-devel python3-pip -y 

3) install docker-composse from python packages (from pypi.org)

python3 -m pip install --upgrade pip setuptools
pip3 install --upgrade pip
pip3 install docker-compose

4) download awx from git
git clone -b "9.1.1" https://github.com/ansible/awx.git


cd awx/installer/


5) edit inventory: 

change file inventory via this guide: 
https://costales.github.io/posts/awx-9-install/ 

(change)
localhost ansible_connection=local ansible_python_interpreter="/usr/bin/env python3"
postgres_data_dir=/var/lib/pgdocker
awx_official=true
project_data_dir=/var/lib/awx/projects

pg_admin_password=postgrespass@123
admin_password=password

6) install awx via ansible-playbook

ansible-playbook -i inventory install.yml 

7) check the containers 

docker container ls 

8) go to www

http://IP

