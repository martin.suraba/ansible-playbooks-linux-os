provider "aws" {
  region = "us-east-1"
}
resource "aws_instance" "vm" {
  ami           = "ami-070b7c2988d4e2c89"
  subnet_id     = "subnet-077541acfde02f87b"
  instance_type = "t3.micro"
  tags = {
    Name = "my-first-tf-node"
  }
}


# acloud guru