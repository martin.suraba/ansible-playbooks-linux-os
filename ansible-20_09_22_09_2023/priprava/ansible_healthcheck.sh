#!/bin/bash 

#time for ansible healthcheck 

ansible all -a "free -h"

ansible all -a "df -hT"

ansible all -a "uptime"

ansible all -a "cat /etc/os-release"

ansible all -a "ip a s"

ansible all -a "ip r s"

ansible all -a "systemctl status sshd"
