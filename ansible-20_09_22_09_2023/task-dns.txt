task: upravte konfiguraciu dns na strojoch pomocou ansible

   40  cd /etc/ansible 
   41  mkdir kody
   42  cp /etc/resolv.conf kody/
   43  ll kody/
   45  vi kody/resolv.conf
   46  cat kody/resolv.conf
   47  ping -c 3 9.9.9.4  -- NEREAGUJE
   48  ping -c 3 8.8.8.8 
   49  vi kody/resolv.conf - zakomentovali sme v 9.9.9.4  
   50  ansible all -m copy -a src=/etc/ansible/kody/resolv.conf dest=/etc/resolv.conf  -- chybaju uvodzovky
   51  ansible all -m copy -a "src=/etc/ansible/kody/resolv.conf dest=/etc/resolv.conf"
   52  ssh server1 cat /etc/resolv.conf
